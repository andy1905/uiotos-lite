# UIOTOS前端零代码开源学习版

#### Description
这是一款WEB前端零代码工具，拥有独特的页面嵌套、属性继承、节点连线特性，与常见低代码、组态等拖拽工具不同，专注于无代码搭建复杂的前端应用。用户无需了解任何前端开发技术（js、css、html、vue等等），通过拖拽嵌套、连线配置，就能增量化搭建复杂的业务应用，包括界面、交互、接口、逻辑、解析等。前后端分离，非常适合后端/硬件/实施工程师，定制开发上层界面应用，比如物联网平台上层业务应用等。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
