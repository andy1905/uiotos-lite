# UIOTOS前端零代码开源学习版

#### 介绍
这是一款WEB前端零代码工具，拥有独特的页面嵌套、属性继承、节点连线特性，与常见低代码、组态等拖拽工具不同，专注于无代码搭建复杂的前端应用。用户无需了解任何前端开发技术（js、css、html、vue等等），通过拖拽嵌套、连线配置，就能增量化搭建复杂的业务应用，包括界面、交互、接口、逻辑、解析等。前后端分离，非常适合后端/硬件/实施工程师，定制开发上层界面应用，比如物联网平台上层业务应用等。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
